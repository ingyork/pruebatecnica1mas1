package co.a1mas1.app.view.convert;

/**
 * Created by Jorge Castrp on 11/03/2018.
 */

public interface IConvertView {
    void ConvertCash();
    void ShowResultConvert(double result);
}
